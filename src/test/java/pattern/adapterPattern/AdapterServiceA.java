package pattern.adapterPattern;

public class AdapterServiceA {
    ServiceA serviceA = new ServiceA();
    void runService() {
        this.serviceA.runServiceA();
    }
}
