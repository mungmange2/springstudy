package pattern.adapterPattern;

public class ClientWithNoAdapter {
    public static void main(String[] args) {
//        ServiceA serviceA = new ServiceA();
//        ServiceB serviceB = new ServiceB();
//        serviceA.runServiceA();
//        serviceB.runServiceB();

        /**
         * 어댑터 패턴
         * - 개방 폐쇄 원칙을 활용한 설계 패턴 (JDBC, JRE가 어댑터의 역할을 수행)
         * - 합성, 즉 객체를 속성으로 만들어서 참조하는 디자인 패턴
         * - 호출당하는 쪽의 메서드(Service)를 호출하는 쪽의 코드(ClientWithNoAdapter)에 대응하도록 중간에 변환기(AdapterService)를 통해 호출하는 패턴
         *
         * 개방 폐쇄 원칙?
         * - 소프트웨어 개체(클래스, 모듈, 함수)는 확장에 대해 열려있어야 하고, 수정에 대해서는 닫혀 있어야 한다.
         * - 내가 만든 로직을 공통화 하여 다른사람들도 갖다쓸 수 있도록 해야한다는 말인듯.
         * */
        AdapterServiceA adapterServiceA = new AdapterServiceA();
        AdapterServiceB adapterServiceB = new AdapterServiceB();
        adapterServiceA.runService();
        adapterServiceB.runService();
    }
}
