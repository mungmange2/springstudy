package pattern.adapterPattern;

public class AdapterServiceB {
    ServiceB serviceB = new ServiceB();
    void runService() {
        this.serviceB.runServiceB();
    }
}
