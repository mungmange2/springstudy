<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="../header.jsp"%>
<body>
<div class="row">
    <div class="grid-12">
        <h3 style="margin-top: 10px;display: inline-list-item;width: 100px;display:inline-table;">회원가입</h3>
        <button class="btn btn--hoverInfo" style="float: right;margin-bottom: 5px;">회원가입</button>
    </div>
    <div class="grid-12">
        <div class="row--align-center">
            <form method="post" action="/user/join">
                <fieldset class="inline-child">
                    <input type="text" name="userId" id="userId" placeholder="아이디" tabindex="1" class="input--grid-12"/>
                </fieldset>

                <fieldset class="inline-child">
                    <input type="password" name="password" id="password" placeholder="비밀번호" tabindex="1" class="input--grid-12"/>
                </fieldset>

                <fieldset class="inline-child">
                    <input type="email" name="email" id="email" placeholder="이메일" class="input--grid-12"/>
                </fieldset>

                <fieldset>
                    <button class="btn btn--positive" type="submit">회원가입</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>
