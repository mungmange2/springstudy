<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="../header.jsp"%>
<body>
<div class="row">
    <div class="grid-12">
        <h3 style="margin-top: 10px;display: inline-list-item;width: 100px;display:inline-table;">로그인</h3>
        <button class="btn btn--hoverInfo" style="float: right;margin-bottom: 5px;" id="join">회원가입</button>
    </div>
    <div class="grid-12">
        <div class="row--align-center">
            <form method="post" action="/user/login">
                <fieldset class="inline-child">
                    <input type="text" name="userId" id="userId" placeholder="아이디" tabindex="1" class="input--grid-12"/>
                </fieldset>

                <fieldset class="inline-child">
                    <input type="password" name="password" id="password" placeholder="비밀번호" tabindex="1" class="input--grid-12"/>
                </fieldset>

                <fieldset>
                    <button class="btn btn--positive" type="submit">로그인</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function() {
        $("#join").on("click", function () {
            location.href="/join";
        });
    });
</script>

</body>
</html>
