<%--
  Created by IntelliJ IDEA.
  User: sohyunpark
  Date: 21. 2. 2.
  Time: 오후 2:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>KakaoPay 이용하기</title>
</head>
<body>
카카오페이 결제가 정상적으로 완료되었습니다.

결제일시:     [[${info.approved_at}]]<br/>
주문번호:    [[${info.partner_order_id}]]<br/>
상품명:    [[${info.item_name}]]<br/>
상품수량:    [[${info.quantity}]]<br/>
결제금액:    [[${info.amount.total}]]<br/>
결제방법:    [[${info.payment_method_type}]]<br/>

</body>
</html>
