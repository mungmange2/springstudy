package com.my.study.service;

import com.my.study.controller.user.dto.UserDto;
import com.my.study.entity.User;
import com.my.study.persist.UserPersist;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
@AllArgsConstructor
public class JoinService {

    private final UserPersist userPersist;
    private final PasswordEncoder passwordEncoder;

    public void saveUser(UserDto.JoinRequest request) {
        User user = new User();
        BeanUtils.copyProperties(request, user);
        user.setGrade(request.getGrade());
        user.setCreated(LocalDateTime.now());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        this.userPersist.saveUser(user);
    }
}
