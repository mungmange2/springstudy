package com.my.study.service;

import com.my.study.annotation.ExecutionTime;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    @ExecutionTime
    public String test() {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<10; i++) {
            sb.append(i);
            sb.append("번째\n");
        }
        return sb.toString();
    }
}
