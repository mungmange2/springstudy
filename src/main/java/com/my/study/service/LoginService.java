package com.my.study.service;

import com.my.study.config.PrincipalDetails;
import com.my.study.entity.User;
import com.my.study.persist.UserPersist;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;

@Slf4j
@Service
public class LoginService implements UserDetailsService {

    @Autowired private UserPersist userPersist;

    /**
     * 로그인 처리
     * */
    @Override
    public PrincipalDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        User user = this.userPersist.readUser(userId);
        if (ObjectUtils.isEmpty(user)) {
            throw new UsernameNotFoundException("User not authorized.");
        }

        return new PrincipalDetails(
                user.getUserId(), user.getPassword(), Arrays.asList(String.valueOf(user.getGrade())));
    }
}
