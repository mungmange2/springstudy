package com.my.study.service;

import java.net.URI;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.my.study.controller.pay.dto.KakaoPayDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KakaoPayService {
    private static final String HOST = "https://kapi.kakao.com";

    @Autowired private RestTemplate restTemplate;
    private KakaoPayDto.ReadyResponseDto readyResponseDto;
    private KakaoPayDto.ApprovalResponseDto approvalResponseDto;

    public String kakaoPayReady() {

        // 서버로 요청할 Header
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 16b3da32e6b9cfce0119d9bd35f81497");
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        // 서버로 요청할 Body
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("cid", "TC0ONETIME");
        params.add("partner_order_id", "1001");
        params.add("partner_user_id", "testsohyun");
        params.add("item_name", "테스트소현");
        params.add("quantity", "1");
        params.add("total_amount", "100");
        params.add("tax_free_amount", "0");
        params.add("approval_url", "http://localhost:8080/pay/kakaoPaySuccess");
        params.add("cancel_url", "http://localhost:8080/pay/kakaoPay/cancel");
        params.add("fail_url", "http://localhost:8080/pay/kakaoPay/fail");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);

        URI uri = UriComponentsBuilder.fromUriString(HOST + "/v1/payment/ready").buildAndExpand().toUri();
        try {
            readyResponseDto = this.restTemplate.postForObject(uri, request, KakaoPayDto.ReadyResponseDto.class);
            log.info("readyResponseDto::{}", readyResponseDto);
            return readyResponseDto.getNext_redirect_pc_url();

        } catch (RestClientException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "/pay/kakaoPay";
    }


    public KakaoPayDto.ApprovalResponseDto kakaoPayInfo(String pg_token) {

        // 서버로 요청할 Header
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "KakaoAK 16b3da32e6b9cfce0119d9bd35f81497");
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        // 서버로 요청할 Body
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("cid", "TC0ONETIME");
        params.add("tid", readyResponseDto.getTid());
        params.add("partner_order_id", "1001");
        params.add("partner_user_id", "testsohyun");
        params.add("pg_token", pg_token);
        params.add("total_amount", "1");

        HttpEntity<MultiValueMap<String, String>> body = new HttpEntity<>(params, headers);
        URI uri = UriComponentsBuilder.fromUriString(HOST + "/v1/payment/approve").buildAndExpand().toUri();

        try {
            approvalResponseDto = this.restTemplate.postForObject(uri, body, KakaoPayDto.ApprovalResponseDto.class);
            log.info("approvalResponseDto::{]", approvalResponseDto);
            return approvalResponseDto;

        } catch (RestClientException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }
}
