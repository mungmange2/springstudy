package com.my.study.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class InvalidData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    @Enumerated(EnumType.STRING)
    private Type targetType;
    private String targetParam;
    private Integer failCount;
    private Date created;
    private Date locked;

    public enum Type {
        USER_ID, UID
    }
}
