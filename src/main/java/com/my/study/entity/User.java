package com.my.study.entity;

import com.my.study.constant.Grade;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private String userId;
    private String password;
    private String email;
    private LocalDateTime joindate;
    @Enumerated(EnumType.STRING)
    private Grade grade;
    private Boolean status;
    private LocalDateTime created;
}
