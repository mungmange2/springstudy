package com.my.study.persist;

import com.my.study.annotation.ExecutionTime;
import com.my.study.entity.User;
import com.my.study.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class UserPersist {

    private final UserRepository userRepository;

    public User readUser(String userId) {
        return this.userRepository.findAllByUserId(userId);
    }

    public void saveUser(User user) {
        this.userRepository.save(user);
    }
}
