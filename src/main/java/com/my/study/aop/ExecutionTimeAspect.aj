package com.my.study.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.logging.Logger;

@Slf4j
@Component
@Aspect
public class ExecutionTimeAspect {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(ExecutionTimeAspect.class);

    @Around("@annotation(com.my.study.annotation.ExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object[] objects = proceedingJoinPoint.getArgs();
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = signature.getMethod();
        StringBuilder sb = new StringBuilder();
        sb.append("[methodName: ");
        sb.append(method.getName());
        sb.append("] ");
        for (int i = 0; i < method.getParameters().length; i++) {
            sb.append(method.getParameters()[i].getName());
            sb.append(": ");
            sb.append(objects[i]);
            sb.append(" ");
        }

        long start = System.currentTimeMillis();
        Object proceed = proceedingJoinPoint.proceed();
        long end = System.currentTimeMillis();
        LOGGER.info(sb+" 소요 시간: "+(end - start)/1000F+"초");
        return proceed;
    }
}