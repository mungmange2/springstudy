package com.my.study.controller.pay;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.my.study.service.KakaoPayService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/pay")
@RequiredArgsConstructor
public class KakaoPayController {

    private final KakaoPayService kakaoPayService;

    @PostMapping("/kakaoPay")
    public String kakaoPay() {
        log.info("kakaoPay post 호출");
        return "redirect:" + this.kakaoPayService.kakaoPayReady();

    }

    @GetMapping("/kakaoPaySuccess")
    public void kakaoPaySuccess(@RequestParam("pg_token") String pg_token, Model model) {
        log.info("kakaoPay 완료 후 페이지");
        log.info("kakaoPaySuccess pg_token::{}", pg_token);
        model.addAttribute("info::{}", this.kakaoPayService.kakaoPayInfo(pg_token));
    }
}
