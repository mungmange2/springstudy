package com.my.study.controller.pay.dto;

import java.time.LocalDateTime;

import lombok.Data;

public class KakaoPayDto {

    @Data
    public static class ReadyResponseDto {
        //response
        private String tid;
        private String tms_result;
        private String next_redirect_app_url;
        private String next_redirect_mobile_url;
        private String next_redirect_pc_url;
        private String android_app_scheme;
        private String ios_app_scheme;
        private LocalDateTime created_at;
    }

    @Data
    public static class ApprovalResponseDto {
        //response
        private String tid, cid;
        private String partner_order_id, partner_user_id, payment_method_type;
        private String item_name;
        private Integer quantity;
        private AmountVO amount;
//        private CardVO card_info;
        private LocalDateTime created_at, approved_at;

        @Data
        public class AmountVO {
            private Integer total, tax_free, vat, point, discount;
        }

        @Data
        public class CardVO {
            private String purchase_corp, purchase_corp_code;
            private String issuer_corp, issuer_corp_code;
            private String bin, card_type, install_month, approved_id, card_mid;
            private String interest_free_install, card_item_code;
        }
    }
}
