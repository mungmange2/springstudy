package com.my.study.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
public class ViewController {

    @GetMapping("/login")
    public String loginView(HttpServletRequest httpServletRequest) {
        log.info("session::{}", httpServletRequest.getSession());
        return "/user/login";
    }

    @GetMapping("/join")
    public String joinView() {
        return "/user/join";
    }

    @GetMapping("/")
    public String mainView(Authentication authentication, HttpServletRequest httpServletRequest) {
        return "/index";
    }
}
