package com.my.study.controller.user;

import com.my.study.controller.user.dto.UserDto;
import com.my.study.service.JoinService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final JoinService joinService;

    @PostMapping(value = "/join")
    public String createUser(@ModelAttribute UserDto.JoinRequest request) {
        this.joinService.saveUser(request);
        return "redirect:/login";
    }
}
