package com.my.study.controller.user.dto;

import com.my.study.constant.Grade;
import lombok.Data;

import java.time.LocalDateTime;

public class UserDto {

    @Data
    public static class JoinRequest {
        private String userId;
        private String password;
        private String email;
        private LocalDateTime joindate = LocalDateTime.now();
        private Grade grade = Grade.GENERAL;
        private Boolean status = true;
    }

    @Data
    public static class LoginRequest {
        private String userId;
        private String password;
    }
}
